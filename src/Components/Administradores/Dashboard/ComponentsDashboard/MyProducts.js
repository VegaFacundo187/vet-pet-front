import React, { Component } from 'react';
import {Link} from "react-router-dom";
import './MyProducts.css';
import handleError from "../../../../Helpers/HandleError";

export default class MyProducts extends Component {

  constructor(props) {
    super(props)
    this.state = {
      cartProducts: []
    }
  }


  componentDidMount = () => {
    let status;
    fetch('http://localhost:4000/products/search?page=0&quantity=10')
      .then(resp => {
        if (!resp.ok) status = resp.status;
        return resp.json();
      })
      .then(resp => {
        if (status) {
          handleError(resp);
          return;
        }
        this.setState({
          cartProducts: resp.prods
        })
      })
      .catch(error => {
        console.log(error);
      });

  }


  render() {

    const myProducts = this.state.cartProducts.map((item, i) => (

      <tr key={i} className={item.featured ? 'table-destacado' : ''}>
        <th scope="row"> {item.prodName} {item.featured ? '🐾' : ''} </th>
        <td><img alt="imagen de producto" className="w-50 table-img widthImg" src={item.image}/></td>
        <td>{item.discount} %</td>
        <td>${item.price}</td>
        <td>{item.stock}</td>
        <td>
          <div  className="d-flex flex-column justify-content-center">
            <Link to={`/admin/deleteproducts/${item._id}`}><button className="btn btn-danger mb-2 table-btn">Borrar</button></Link>
            <Link to={`/admin/editproducts/${item._id}`}><button className="btn btn-success table-btn">Editar</button></Link>
          </div>
        </td>
      </tr>
    ))

    return (
      <>
        <h2 className="text-center my-5">Mis productos</h2>
        <div className="table-responsive">
        <table className="table table-hover table-prods">
          <thead>
            <tr>
              <th scope="col text-center">Titulo</th>
              <th scope="col">Imagen</th>
              <th scope="col">Descuento</th>
              <th scope="col">Precio</th>
              <th scope="col">Stock</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody className="widthImg">
            {myProducts}
          </tbody>
        </table>
        </div>
      </>
    )
  }
}
