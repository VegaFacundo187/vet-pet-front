import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class componentName extends Component {

  constructor(props) {
    super(props)
    this.state = {
      consultas: undefined
    }
  }

  componentDidMount() {
    fetch(`http://localhost:4000/faq/getallfaq`)
      .then(resp => resp.json())
      .then(response => {
        this.setState({ consultas: response });
      });
  }

  
  render() {
    return (
      <>
        <div className="container-fluid py-4 text-center">
          <h1>Mis mensajes</h1>
        </div>
        <table className="table table-hover ">
          <thead>
            <tr>
              <th scope="col">Usuario</th>
              <th scope="col">Mensaje</th>
            </tr>
          </thead>
          <tbody>
            {this.state.consultas ? this.state.consultas.Faqs.map((item, i) =>
              <tr key={i}>
                <td className="">{item.email}</td>
                <td className="">
                  <Link to={`/admin/messages/${item._id}`} className="d-block">
                    {item.inquire}
                  </Link>
                </td>
              </tr>
            ) : <tr></tr>}
          </tbody>
        </table>
      </>
    )
  }
}
