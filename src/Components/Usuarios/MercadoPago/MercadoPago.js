import React, { Component } from 'react';
import './MercadoPago.css';

export default class MercadoPago extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nombre: '',
      apellido: '',
      email: '',
      titular: '',
      tarjeta: '',
      cds: '',
      vencimineto: '',
      regexp: /^[a-z\b]+$/,
      regexp1: /^[0-9\b]+$/
    }
  }

  handleInput = e => {
    const { value, name } = e.target;
    this.setState({
      [name]: value
    });
    console.log(this.state)
  };

  alertas = (hasError) => {
    const n = this.state.nombre
    if (this.state.nombre === '' || this.state.regexp1.test(n)) {
      alert('ajsfh');
      hasError = true
    } else {
      alert('2')
    }
    if (this.state.apellido === '' || this.state.regexp1.test(this.state.apellido)) {
      alert('ajsfh');
      hasError = true
    } else {
      alert('2')
    }
    if (this.state.email === '' || this.state.regexp1.test(this.state.email)) {
      alert('ajsfh');
      hasError = true
    } else {
      alert('2')
    }
    if (this.state.titular === '' || this.state.regexp1.test(this.state.titular)) {
      alert('ajsfh');
      hasError = true
    } else {
      alert('2')
    }
    if (this.state.tarjeta === '' || this.state.tarjeta.length > 16 || this.state.regexp.test(this.state.tarjeta)) {
      alert('ajsfh');
      hasError = true
    } else {
      alert('2')
    }
    if (this.state.cds === '' || this.state.cds.length > 3 || this.state.regexp.test(this.state.cds)) {
      alert('ajsfh');
      hasError = true
    } else {
      alert('2')
    }
    return hasError;
  }

  handleFetch = () => {
    console.log('twit');
  }

  handleSubmit = () => {
    let hasError = false;
    hasError = this.alertas(hasError);
    if (!hasError) {
      this.handleFetch();
    }
  }
  render() {

    const coleccionCosas = JSON.parse(localStorage.getItem('CarritoItems')) || [];
    const CartTable = Array.isArray(coleccionCosas) && coleccionCosas.map((item, i) => {
      let precio = item.price - ((item.price / 100) * item.discount)
      return (
        <tr>
          <th colSpan="1" scope="row" className="text-center">
            <img alt="imagen del producto" src={item.image} className="img-fluid img" />
          </th>
          <td colSpan="4">{item.prodName}</td>
          <td>x{item.quantity}</td>
          <td>${precio}</td>
          <td>{item.discount}%</td>
        </tr>
      )
    }
    );

    return (
      <div className="container">
        <div className="row justify-content-around bckgMercadoPago">
          <div className="col-7 inpMercadoPago">
            <div className="input-group d-flex justify-content-between align-content-center mb-4">
              <div className="input-group-prepend col-12 col-md-5 p-0 mb-3">
                <span className="input-group-text">Nombre</span>
                <input type="text" name="nombre" onChange={this.handleInput} className="form-control" />
              </div>
              <div className="input-group-prepend col-12 col-md-5 p-0 mb-3">
                <span className="input-group-text">Apellido</span>
                <input type="text" name="apellido" onChange={this.handleInput} className="form-control" />
              </div>
            </div>
            <div className="input-group mb-5">
              <div className="input-group-prepend">
                <span className="input-group-text">Direccion</span>
              </div>
              <input type="email" name="email" onChange={this.handleInput} className="form-control" />
            </div>
            <div className="input-group mb-5">
              <div className="input-group-prepend">
                <span className="input-group-text">Titular</span>
              </div>
              <input type="text" name="titular" onChange={this.handleInput} className="form-control" />
            </div>
            <div className="input-group d-flex justify-content-between mb-3">
              <div className="input-group-prepend col-12 col-md-5 p-0 mb-3">
                <span className="input-group-text">N° Tarjeta</span>
                <input type="text" maxLength="3" name="tarjeta" onChange={this.handleInput} className="form-control" />
              </div>
              <div className="input-group-prepend col-12 col-md-2 p-0 mb-3">
                <span title="Codigo de seguridad" className="input-group-text">CDS</span>
                <input type="number" name="cds" onChange={this.handleInput} className="form-control" />
              </div>
              <div className="input-group-prepend col-12 col-md-3 p-0 mb-3">
                <span className="input-group-text">Vnct</span>
                <input type="number" name="vencimineto" onChange={this.handleInput} className="form-control" />
              </div>
            </div>
            <button className="btn btn-primary" onClick={this.handleSubmit}>Comprar</button>
          </div>
          <div className="col-12 col-md-5 align-self-center">
            <div class="table-responsive tableHeight mb-3">
              <table className="table">
                <thead>
                  <tr>
                    <th scope="row"></th>
                    <th colSpan="4">Tu Carrito</th>
                    <th scope="col">C/D</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Desc</th>
                  </tr>
                </thead>
                <tbody className="tableScroll widthImg">
                  {CartTable}
                </tbody>
              </table>
            </div>
            <img alt="banner mercadopago" src="https://dia8publicidad.com/wp-content/uploads/2016/10/vs1.jpg" className="img-fluid mt-3"></img>
          </div>
        </div>
      </div>
    )
  }
}
