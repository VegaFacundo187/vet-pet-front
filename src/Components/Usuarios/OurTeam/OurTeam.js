import React from "react";
import "../OurTeam/OurTeam.css";

const dataOurTeam = [
  {
    title: "Lic. Ramos, Cristina",
    img:
      "images/dra2.jpg",
    text: "Esp. Felinos"
  },
  {
    title: "Dr. Gómez, Pablo",
    img:
      "images/drveet.jpg",
    text: "Radiología"
  },
  {
    title: "Lic. Arrayán, Marina",
    img:
      "images/dra.jpg",
    text: "Veterinaria Clinica"
  },
  {
    title: "Lic. Andreosi, German",
    img:
      "images/drvet.jpg",
    text: "Esp. Traumatologia Animal"
  }
];

function OurTeam(props) {
  const ourteam = dataOurTeam.map((data, i) => (
    <div key={i} className="col-lg-3 col-md-6 col-12">
      <figure className="hover01">
        <img
          alt="foto del profesional"
          src={data.img}
          className="card-img-top circledimg img-team"
        />
      </figure>
      <div className="card-body">
        <h5 className="card-title">{data.title}</h5>
        <p className="card-text">{data.text}</p>
      </div>
    </div>
  ));

  return (
    <React.Fragment>
      <div className="banner" id="ourteam">
        <h1 className="text-center ">NUESTRO EQUIPO DE PROFESIONALES</h1>
      </div>
      <div className="container-ourteam container mt-5">
        <div className="row text-center">{ourteam}</div>
      </div>
    </React.Fragment>
  );
}

export default OurTeam;
