import React, { useEffect, useState } from 'react';
import { Redirect } from "react-router-dom";
import './ConfirmacionCompra.css';

export default function ConfirmacionCompra() {

  let [compra, setCompra] = useState(false)

  // useEffect(() => {
  //   const querystring = window.location.search;
  //   const params = new URLSearchParams(querystring)
  //   console.log(params.get('collection_id'));
  //   fetch(`http://localhost:4000/sales/updateStock?collection_id=${params.get('collection_id')}`)
  //     .then(resp => resp.json())
  //     .then(data => console.log("Holaaa"))
  //     setTimeout(() => {
  //       setCompra(true);},
  //       5000);
  // }, []);

  useEffect(() => {    
    const querystring = window.location.search;
    const params = new URLSearchParams(querystring);
    const collectionId = params.get('collection_id');
    console.log(params.get('collection_id'));
  
    fetch('http://localhost:4000/sales/updateStock', {
      method: 'PUT',
      body: JSON.stringify({ collection_id: collectionId}),  
      headers: {
        'Content-Type': 'application/json',
        // 'Authorization': `Bearer ${token}`,
      },
    })
    .then(resp => resp.json())
    .then(res => console.log("La reees ",res))
    setTimeout(() => {
            setCompra(true);},
            5000);
    console.log("Dentro de useEffect ", compra);
    },[compra]);

  console.log('valor',compra)
  if(compra){
     return <Redirect to="/" /> 
  }else{
  return (
    <React.Fragment>
      <div className="container-fluid contactopart pl-5 pb-5">
        <div className="row mt-5 justify-content-center">
          <div className="text-center pt-4">
            <p>Tu compra fue realizada con Exito!</p>
          </div>
          <div className="pt-4 pr-5 col-6 contacto-text text-center">
            <p><img  alt="imagen de confirmacion" className="img-fluid" src="https://images-na.ssl-images-amazon.com/images/I/71BEBk0uEOL._AC_SX425_.jpg"></img></p>
            <div className="contacto-firma row float-right">
              <img  alt="logo veterinaria patitas" src={"/images/LOGO_VETE.svg"} />
              <h4 className="ml-3"> PATITAS</h4>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );  }
};