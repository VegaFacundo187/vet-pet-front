import React from "react";
import "../Turnos/Turnos.css";
import moment from "moment";
import { Redirect } from "react-router-dom";
import handleError from "./../../../Helpers/HandleError";

export default class Turnos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nombreMascota: "",
      horario: "",
      fecha: "",
      availables: [],
      redirect: undefined
    };
  }
  horarios = ["08:00", "09:00", "10:00", "11:00", "12:00"];

  handleTurnoDispo = date => {
    let status;
    if (!date) {
      this.setState({ availables: [] });
      return;
    }
    if (date <= moment().format("YYYY-MM-DD")) {
      alert("Fecha inválida");
      return;
    }
    const token = JSON.parse(localStorage.getItem("token"));
    fetch(`http://localhost:4000/appointments/${date}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    })
      .then(resp => {
        if (!resp.ok) status = resp.status;
        return resp.json();
      })
      .then(resp => {
        if (status) {
          handleError(resp);
          return;
        }
        this.listAvailables(resp.appointments);
      });
  };

  listAvailables(occupiedAppos) {
    const horarios = [...this.horarios];
    occupiedAppos.forEach(occupiedAppo => {
      const index = horarios.findIndex(
        horario => moment(occupiedAppo.startTime).format("HH:mm") === horario
      );
      !isNaN(index) && horarios.splice(index, 1);
    });
    this.setState({ availables: horarios });
  }

  handleTurno = newAppo => {
    if (newAppo <= moment().format("YYYY-MM-DD")) {
      alert("Fecha inválida");
      return;
    }
    let status;
    const token = JSON.parse(localStorage.getItem("token"));
    fetch(`http://localhost:4000/appointments`, {
      method: "POST",
      body: JSON.stringify(newAppo),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    })
      .then(resp => {
        if (!resp.ok) status = resp.status;
        return resp.json();
      })
      .then(resp => {
        if (status) {
          handleError(resp);
          return;
        }
        alert(
          `¡TURNO CONFIRMADO!  ${moment(newAppo.startTime).format(
            "HH:mm  DD-MM-YYYY"
          )}`
        );
        this.setState({ redirect: "/" });
      });
  };

  handleInput = e => {
    if (!JSON.parse(localStorage.getItem("token"))) {
      alert("Debe ingresar con su cuenta para solicitar un turno");
      this.setState({ islogged: false });
      return;
    }
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: value
    });
  };

  handleInputDate = e => {
    if (!JSON.parse(localStorage.getItem("token"))) {
      alert("Debe ingresar con su cuenta para solicitar un turno");
      this.setState({ islogged: false });
      return;
    }
    this.handleInput(e);
    this.handleTurnoDispo(e.target.value);
  };

  handleSubmit = e => {
    e.preventDefault();
    if (!JSON.parse(localStorage.getItem("token"))) {
      alert("Debe ingresar con su cuenta para solicitar un turno");
      this.setState({ redirect: "/login" });
      return;
    }
    if (!this.state.fecha || !this.state.horario || !this.state.nombreMascota) {
      alert("Debe completar todos los campos");
      return;
    }

    this.handleTurno({
      startTime: this.state.fecha + " " + this.state.horario,
      description: this.state.nombreMascota
    });
  };

  render() {

    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <React.Fragment>
        <div className="container">
          <div className="row  mt-2 justify-content-center  ">
            <div className="turnospart">
              <div className=" mx-5 pt-4 text-center" id="turnos">
                <p>TURNOS PELUQUERIA</p>
              </div>
              <form onSubmit={this.handleSubmit} className="m-5 pb-5  ">
                <div className="justify-content">
                  <div className="col-12  mb-3">
                    <h5>NOMBRE DE TU MASCOTA</h5>
                    <input
                      type="text"
                      className="form-control formturnosize"
                      name="nombreMascota"
                      onChange={this.handleInput}
                      placeholder="Nombre de tu Mascota"
                    />
                  </div>
                  <div className="col-12  mb-3">
                    <h5>FECHA MES EN CURSO</h5>
                    <input
                      type="date"
                      className="form-control formturnosize"
                      name="fecha"
                      onChange={this.handleInputDate}
                      placeholder="Nombre de tu Mascota"
                      min={moment()
                        .add(1, "days")
                        .format("YYYY-MM-DD")}
                    />
                  </div>
                  <div className="col-12  mb-3">
                    <h5>HORARIO</h5>
                    <select
                      className="custom-select selectcss formturnosize select mr-sm-2"
                      name="horario"
                      onChange={this.handleInput}
                      id="inlineFormCustomSelect"
                    >
                      <option defaultValue value="">
                        Seleccionar Hora
                      </option>
                      {this.state.availables.map((horario, i) => (
                        <option key={i} value={horario}>
                          {horario}
                        </option>
                      )) || ""}
                    </select>
                  </div>
                  <div className="col-12 text-right mt-4 mt-md-2">
                    <button type="submit" className="btn sharedbtn mb-2   ">
                      <span>Solicitar </span>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
