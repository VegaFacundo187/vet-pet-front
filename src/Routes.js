import React from "react";
import Login from "./Components/Usuarios/Login/Login";
import Admin from "./Components/Administradores/Admin/Admin";
import CommonNavbar from "./Components/Usuarios/CommonNavbar/CommonNavbar";
import Slider from "./Components/Usuarios/Slider/Slider";
import MiniMarket from "./Components/Usuarios/MiniMarket/MiniMarket";
import MainNavbar from "./Components/Usuarios/MainNavbar/MainNavbar";
import Contacto from "./Components/Usuarios/Contacto/Contacto";
import OurTeam from "./Components/Usuarios/OurTeam/OurTeam";
import ConfirmacionCompra from "./Components/Usuarios/ConfirmacionCompra/ConfirmacionCompra";
import Cart from "./Components/Usuarios/Cart/Cart";
import Regist from "./Components/Usuarios/Regist/Regist";
import MercadoPago from "./Components/Usuarios/MercadoPago/MercadoPago";
import Market from "./Components/Usuarios/Market/Market";
import Turnos from "./Components/Usuarios/Turnos/Turnos";
import Article from "./Components/Usuarios/Article/Article";
import NavLinksBar from "./Components/Usuarios/NavLinksBar/NavLinksBar";
import Servicios from "./Components/Usuarios/Servicios/Servicios";
import Footer from "./Components/Usuarios/Footer/Footer";
import Emergencias from "./Components/Usuarios/Emergencias/Emergencias";
import PrivateRoute from "./Auth";
import SocialMediaMIndex from "./Components/Usuarios/SocialMediaMIndex/SocialMediaMIndex";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

export default class Routes extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/login" component={Login} />
          <PrivateRoute path="/admin" component={Admin} />
          <Route path="/cart">
            <Emergencias />
            <CommonNavbar />
            <Cart />
            <Footer />
          </Route>
          <Route path="/market">
            <Emergencias />
            <SocialMediaMIndex />
            <CommonNavbar />
            <Market />
            <Footer />
          </Route>
          <Route path="/regist">
            <Emergencias />
            <CommonNavbar />
            <Regist />
            <Footer />
          </Route>
          <Route path="/equipo">
            <Emergencias />
            <SocialMediaMIndex />
            <CommonNavbar />
            <OurTeam />
            <Footer />
          </Route>
          <Route path="/confirmacionCompra">
            <CommonNavbar />
            <ConfirmacionCompra />
            <Footer />
          </Route>
          <Route path="/contacto">
            <Emergencias />
            <CommonNavbar />
            <Contacto />
            <Footer />
          </Route>
          <Route path="/servicios">
            <Emergencias />
            <SocialMediaMIndex />
            <CommonNavbar />
            <Servicios />
            <Footer />
          </Route>
          <Route path="/turnos">
            <Emergencias />
            <CommonNavbar />
            <Turnos />
            <Footer />
          </Route>
          <Route path="/article/:id">
            <Emergencias />
            <SocialMediaMIndex />
            <CommonNavbar />
            <Article />
            <Footer />
          </Route>
          <Route path="/mercadopago">
            <Emergencias />
            <MercadoPago />
          </Route>
          <Route path="/">
            <Emergencias />
            <SocialMediaMIndex />
            <MainNavbar />
            <NavLinksBar />
            <Slider />
            <MiniMarket />
            <Servicios />
            <OurTeam />
            <Footer />
          </Route>
        </Switch>
      </Router>
    );
  }
}
